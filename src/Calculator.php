<?php

namespace Volnenko\Testing;

class Calculator {

   private static $instance = null;

   public static function getInstance() {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
   }

   public function sum($a, $b){
        return $a + $b;
   }

}