<?php

use PHPUnit\Framework\TestCase;
use Volnenko\Testing\Calculator;

class CalculatorTest extends TestCase {

    public function testSum() {
        $calculator = new Calculator();
        $result = $calculator->sum(1, 2);
        $this->assertSame(3, $result);
    }

    public function testInstance() {
        $instance = Calculator::getInstance();
        $this->assertNotNull($instance);
    }

}